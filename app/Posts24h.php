<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posts24h extends Model
{
  protected $table = 'posts_24h';
  protected $fillable = [
    'date_post','title', 'description', 'image', 'content', 'url','category','created_at', 'updated_at'
  ];
  public $timestamps = false;
}
