<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
  protected $table = 'posts';
  protected $fillable = [
    'date_post','title', 'description', 'image', 'content', 'url','category','created_at', 'updated_at'
  ];
  public $timestamps = false;
}
