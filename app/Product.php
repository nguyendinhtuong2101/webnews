<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
  public function up()
  {
      Schema::create('products', function (Blueprint $table) {
          $table->id();
          $table->string('name');
          $table->integer('price');
          $table->integer('rate');
          $table->timestamps();
      });
  }
}
