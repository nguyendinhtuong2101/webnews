<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Goutte;
use App\Posts24h;
use App\Posts;
class ScrapeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */

    protected $signature = 'scrape:tintuc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $menu_dantri = [
      'su-kien.htm',
      'xa-hoi.htm',
      'the-gioi.htm',
      'the-thao.htm',
      'suc-khoe.htm',
      'kinh-doanh.htm',
      'giao-duc-huong-nghiep.htm',
      'van-hoa.htm',
      'giai-tri.htm',
      'phap-luat.htm',
    ];
    protected $menu_24h = [
    'bong-da-quoc-te',
    'bong-da',
     'lich-thi-dau',

    ];


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

      $linkwebdantri =  "https://dantri.com.vn";
      $linkweb24     =   "https://laodong.vn";

    //  dd($linkweb);
        foreach ($this ->menu_dantri as $menu_dantri ) {
                $ul_menu = $linkwebdantri.'/'.$menu_dantri;
                $category = str_replace(".htm","",$menu_dantri);
              //  print_r($ul_menu);
                $crawler = Goutte::request('GET', $ul_menu);
                $menu    =  $crawler->filter('h3.news-item__title a')->each(function ($node) {
                        return $node->attr('href');
                  });
                //  dd($menu);
                  foreach ($menu as $value) {
                      $url = $linkwebdantri.$value;
                      self::get_data_web($url,$category);
                  }
            }
            // 24h
          foreach ($this ->menu_24h as $menu_24h ) {
                  $ul_menu24 = $linkweb24.'/'.$menu_24h;

                  $crawler = Goutte::request('GET', $ul_menu24);
                  $menu    =  $crawler->filter('.list-main-content a')->each(function ($node) {
                          return $node->attr('href');
                    });
                    foreach ($menu as $value) {
                        $url = $value;
                       self::get_data_web24h($url,$ul_menu24);
                    }
              }

    }
    public function get_data_web($url,$category){
      date_default_timezone_set("Asia/Ho_Chi_Minh");
      $date = date('Y-m-d H:i:s');

      $crawler = Goutte::request('GET', $url);
        $title       =  $crawler->filter('h1.dt-news__title')->each(function ($node) {
                return $node->text();
          });
        if(isset($title[0]))
        {
          $title = $title[0];
        }else   $title ="";
        $description =  $crawler->filter('.dt-news__sapo h2')->each(function ($node) {
              return $node->text();
        });
        if(isset($description[0]))
        {
          $description = $description[0];
        }else   $description ="";
        $content    =  $crawler->filter('.dt-news__content')->each(function ($node) {
              return $node->text();
        });
        if(isset($content[0]))
        {
          $content = $content[0];
        }else   $content ="";

        $image      =  $crawler->filter('.image img')->each(function ($node) {
              return $node->attr('src');
              //  return $node->text();
        });
        if(isset($image[0]))
        {
          $image = $image[0];
        }else   $image ="";

        $date_create     =  $crawler->filter('.dt-news__meta')->each(function ($node) {
            //  return $node->attr('src');
                return $node->text();
        });
        if(isset($date_create[0]))
        {
          $date_create = $date_create[0];
          $date_create = substr(strrchr($date_create, ', '),2,10 );
          $arr = explode('/', $date_create);
          $date_post = $arr[2].'-'.$arr[1].'-'.$arr[0];

        }else   $date_post = $date;

      //  dd($date_post);

        $data = [
          'date_post'   => $date_post,
          'title'       => $title,
          'description' => $description,
          'image'       => $image,
          'content'     => $content,
          'url'         => $url,
          'category'    => $category,
          'created_at'  => $date,
        ];
      //  dd($data['url']);
        $check =   Posts::where('url',$data['url'])->first();
        if(!$check){
          Posts::create($data);
        }

      //  dump('insert thành công');
    }

    public function get_data_web24h($url,$ul_menu24){
      date_default_timezone_set("Asia/Ho_Chi_Minh");
      $date = date('Y-m-d H:i:s');
      $crawler = Goutte::request('GET', $url);
      if($ul_menu24 != "https://laodong.vn/lich-thi-dau"){
          $title   =  $crawler->filter('.title h1')->each(function ($node) {
                    return $node->text();
              });
        }else{

          $title   =  $crawler->filter('.section-title')->each(function ($node) {
                    return $node->text();
              });
        }
        //  dd($title);

        if(isset($title[0]))
        {
          $title = $title[0];
        }else   $title ="";

        $description =  $crawler->filter('p')->each(function ($node) {
              return $node->text();
        });
      //  dd($description);

        if(isset($description[0]))
        {
          $description = $description[0];
        }else   $description ="";


        $content    =  $crawler->filter('.article-content')->each(function ($node) {
              return $node->text();
        });
        if(isset($content[0]))
        {
          $content = $content[0];
        }else   $content ="";
      //  dd($content);
        $image      =  $crawler->filter('.insert-center-image img')->each(function ($node) {
             return $node->attr('src');
          //   return $node;
        });

        if(isset($image[0]))
        {
          $image = $image[0];
        }else   $image ="";
        //  dd($image);
        $date_create     =  $crawler->filter('.f-datetime')->each(function ($node) {
            //  return $node->attr('src');
                return $node->text();
        });
        //  dd(  $date_create );
        if(isset($date_create[0]))
        {
          $date_create = $date_create[0];
          //  dd($date_create);
          $date_create = substr($date_create,0,10 );
        //  dd($date_create);
          $arr = explode('/', $date_create);
          $date_post = $arr[2].'-'.$arr[1].'-'.$arr[0];

        }else   $date_post = $date;

    //   dd($date_post);

        $data = [
          'date_post'   => $date_post,
          'title'       => $title,
          'description' => $description,
          'image'       => $image,
          'content'     => $content,
          'url'         => $url,
          'category'    => $ul_menu24,
          'created_at'  => $date,
        ];
      //  dd($data['url']);
        $check =   Posts24h::where('url',$data['url'])->first();
        if(!$check){
          Posts24h::create($data);
        }
        DB::table('posts')
       ->orWhere('content', 'like', '%' .'Covid'. '%')
       ->orWhere('title', 'like', '%' .'Covid'. '%')
       ->orWhere('image', '=', '')
       ->delete();

       DB::table('posts_24h')
      ->orWhere('content', 'like', '%' .'Covid'. '%')
      ->orWhere('title', 'like', '%' .'Covid'. '%')
      ->orWhere('image', '=', '')
      ->delete();
        dump('insert thành công');
    }
}
